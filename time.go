package ins

import "time"

var (
	gpsEpoch time.Time = time.Date(1980, time.January, 6, 0, 0, 0, 0, time.UTC)
	// Leap seconds since the GPS epoch
	leapSeconds time.Duration = time.Second * 18
)

const (
	weekLen time.Duration = time.Second * 604800
)

// GpsToUtc converts a GPS week number and offset to a UTC time
func GpsToUtc(gpsweek int64, offset time.Duration) time.Time {
	weeks := time.Duration(gpsweek) * weekLen
	t := gpsEpoch.Add(weeks).Add(offset).Add(-leapSeconds)
	return t
}

// UtcToGps converts a UTC time to a GPS week number and offset
func UtcToGps(t time.Time) (int64, time.Duration) {
	d := t.Sub(gpsEpoch) + leapSeconds
	gpsWeek := d.Truncate(weekLen)
	return int64(gpsWeek / weekLen), d - gpsWeek
}
