package ins

import (
	"bytes"
	"encoding/binary"
	"reflect"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

type hrPos struct {
	Lat int64
	Lon int64
	Alt int32
}

// Map user-data type codes to sizes
var udSizeMap map[uint8]int = map[uint8]int{
	0x01: 4, 0x02: 8, 0x03: 8, 0x04: 9, 0x07: 6, 0x08: 12, 0x09: 8,
	0x10: 12, 0x11: 20, 0x12: 12,
	0x20: 6, 0x21: 12, 0x22: 6, 0x23: 12, 0x24: 6, 0x25: 6, 0x26: 7,
	0x30: 12, 0x31: 20, 0x32: 10, 0x33: 4, 0x34: 6, 0x35: 4, 0x36: 2, 0x37: 8, 0x38: 1,
	0x39: 1, 0x3a: 1, 0x3b: 1, 0x3c: 2, 0x3d: 2, 0x3e: 4, 0x3f: 4,
	0x40: 4, 0x41: 1, 0x42: 10, 0x43: 4, 0x44: 4, 0x45: 2, 0x46: 9,
	0x50: 2, 0x51: 2, 0x52: 2, 0x53: 2, 0x54: 1, 0x55: 3, 0x56: 1, 0x57: 6,
	0x60: 4, 0x61: 2, 0x62: 8, 0x63: 20, 0x64: 16, 0x65: 2, 0x66: 6, 0x67: 24, 0x68: 7,
	0x69: 4,
}

type userDefMessage struct {
	GpsTime   uint64        `udtype:"0x02"`
	ImuTime   uint64        `udtype:"0x03"`
	Angles    [3]int32      `udtype:"0x08"`
	Pos       hrPos         `udtype:"0x11"`
	Vel       [3]int32      `udtype:"0x12"`
	Gyro      [3]int32      `udtype:"0x21"`
	Accel     [3]int32      `udtype:"0x23"`
	Mag       [3]int16      `udtype:"0x24"`
	Bias      [7]int8       `udtype:"0x26"`
	GnssPos   hrPos         `udtype:"0x31"`
	GnssInfo  [2]uint8      `udtype:"0x36"`
	SvInfo    [8]uint8      `udtype:"0x37"`
	GpsWeek   uint16        `udtype:"0x3c"`
	NewGps    uint8         `udtype:"0x41"`
	Temp      int16         `udtype:"0x52"`
	Usw       uint16        `udtype:"0x53"`
	NewAiding uint16        `udtype:"0x65"`
	Aiding    DvlAidingData `udtype:"0x67"`
}

var udSize int = binary.Size(userDefMessage{})

// DataRecord is a user-defined message converted to unscaled units
type DataRecord struct {
	Tsec             int64
	Tnsec            int32
	ImuOffset        int64
	Gyro             [3]float64
	Accel            [3]float64
	GyroBias         [3]float32
	AccelBias        [3]float32
	Mag              [3]float32
	Usw              StatusWord
	Temp             float32
	Heading          float32
	Pitch, Roll      float32
	Lat, Lon         float64
	Alt              float64
	E, N, U          float32
	GnssLat, GnssLon float64
	GnssAlt          float64
	GnssInfo         [2]uint8
	SvInfo           [8]uint8
	NewGps           uint8
	NewAiding        uint16
	Aiding           DvlAidingData
}

// TypeCodes returns a slice of type codes for the INS user defined
// data fields in the struct T along with the total record size
// in bytes
func TypeCodes(T interface{}) ([]uint8, int) {
	var size int
	codes := make([]uint8, 0)
	t := reflect.TypeOf(T)
	if t.Kind() != reflect.Struct {
		return codes, size
	}
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if v, ok := f.Tag.Lookup("udtype"); ok {
			x, err := strconv.ParseUint(v, 0, 8)
			if err == nil {
				codes = append(codes, uint8(x))
				size += udSizeMap[uint8(x)]
			}
		}
	}

	return codes, size
}

// UserRecordSize returns the total record size from the type codes
func UserRecordSize(codes []uint8) int {
	var size int
	for _, code := range codes {
		size += udSizeMap[code]
	}
	return size
}

func (r DataRecord) Timestamp() time.Time {
	return time.Unix(r.Tsec, int64(r.Tnsec)).UTC()
}

func (r *DataRecord) SetTimestamp(t time.Time) {
	ns := t.UnixNano()
	r.Tsec = ns / nsecPerSec
	r.Tnsec = int32(ns % nsecPerSec)
}

// UnpackUserData creates a DataRecord from a User Defined data message payload.
func UnpackUserData(payload []byte) (DataRecord, error) {
	msg := userDefMessage{}
	if len(payload) < udSize {
		return DataRecord{}, errors.Errorf("Bad payload size: %d", len(payload))
	}
	n := payload[0]
	rdr := bytes.NewReader(payload[n+1:])
	err := binary.Read(rdr, ByteOrder, &msg)
	if err != nil {
		return DataRecord{}, err
	}

	week := int64(msg.GpsWeek)
	if week == 0 {
		week, _ = UtcToGps(time.Now())
	}

	rec := DataRecord{
		ImuOffset: int64(msg.ImuTime) - int64(msg.GpsTime),
		Usw:       StatusWord(msg.Usw),
		Heading:   float32(msg.Angles[0]) / ANGLESCALE_HR,
		Pitch:     float32(msg.Angles[1]) / ANGLESCALE_HR,
		Roll:      float32(msg.Angles[2]) / ANGLESCALE_HR,
		Lat:       float64(msg.Pos.Lat) / GEOSCALE_HR,
		Lon:       float64(msg.Pos.Lon) / GEOSCALE_HR,
		Alt:       float64(msg.Pos.Alt) / ALTSCALE_HR,
		GnssLat:   float64(msg.GnssPos.Lat) / GEOSCALE_HR,
		GnssLon:   float64(msg.GnssPos.Lon) / GEOSCALE_HR,
		GnssAlt:   float64(msg.GnssPos.Alt) / ALTSCALE_HR,
		E:         float32(msg.Vel[0]) / SPEEDSCALE,
		N:         float32(msg.Vel[1]) / SPEEDSCALE,
		U:         float32(msg.Vel[2]) / SPEEDSCALE,
		Temp:      float32(msg.Temp) / TEMPSCALE,
		SvInfo:    msg.SvInfo,
		NewGps:    msg.NewGps,
		GnssInfo:  msg.GnssInfo,
		NewAiding: msg.NewAiding,
		Aiding:    msg.Aiding,
	}

	(&rec).SetTimestamp(GpsToUtc(week, time.Duration(msg.GpsTime)))

	for i := 0; i < 3; i++ {
		rec.Gyro[i] = float64(msg.Gyro[i]) / GYROSCALE_HR
		rec.Accel[i] = float64(msg.Accel[i]) / ACCELSCALE_HR
		rec.Mag[i] = float32(msg.Mag[i]) / MAGSCALE
		rec.GyroBias[i] = float32(msg.Bias[i]) / GYROBIASSCALE
		rec.AccelBias[i] = float32(msg.Bias[i+3]) / ACCELBIASSCALE
	}

	return rec, nil
}
