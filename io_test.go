package ins

import (
	"bytes"
	"testing"
)

func TestMsgOut(t *testing.T) {
	table := []struct {
		id           uint8
		payload, out []byte
	}{
		{
			id:      0x62,
			payload: []byte("\x02\x02\x03\xb9\x0b\xe9\x03\x0b\xfe\x34\x00\x2a\x00"),
			out:     []byte("\xaa\x55\x01\x62\x13\x00\x02\x02\x03\xb9\x0b\xe9\x03\x0b\xfe\x34\x00\x2a\x00\x94\x03"),
		},
		{payload: []byte("\x50"), out: []byte("\xaa\x55\x00\x00\x07\x00\x50\x57\x00")},
		{payload: []byte("\x52"), out: []byte("\xaa\x55\x00\x00\x07\x00\x52\x59\x00")},
		{payload: []byte("\x56"), out: []byte("\xaa\x55\x00\x00\x07\x00\x56\x5d\x00")},
		{payload: []byte("\xfe"), out: []byte("\xaa\x55\x00\x00\x07\x00\xfe\x05\x01")},
		{payload: []byte("\xb2"), out: []byte("\xaa\x55\x00\x00\x07\x00\xb2\xb9\x00")},
	}

	var b bytes.Buffer
	w := NewWriter(&b)

	for _, e := range table {
		if e.id != 0 {
			w.SetPacketId(e.id)
		}
		_, err := w.Write(e.payload)
		if err != nil {
			t.Fatal(err)
		}
		msg := b.String()
		if msg != string(e.out) {
			t.Errorf("Output mismatch; expected %v, got %v", string(e.out), msg)
		}
		b.Reset()
	}
}

func TestMsgIn(t *testing.T) {
	table := []struct {
		cmd Command
		out []byte
	}{
		{cmd: 0x50, out: []byte("\xaa\x55\x00\x00\x07\x00\x50\x57\x00")},
		{cmd: 0x52, out: []byte("\xaa\x55\x00\x00\x07\x00\x52\x59\x00")},
		{cmd: 0x56, out: []byte("\xaa\x55\x00\x00\x07\x00\x56\x5d\x00")},
		{cmd: 0xfe, out: []byte("\xaa\x55\x00\x00\x07\x00\xfe\x05\x01")},
	}

	payload := make([]byte, 1)
	for _, e := range table {
		r := NewReader(bytes.NewReader(e.out))
		n, err := r.Read(payload)
		if err != nil {
			t.Fatal(err)
		}

		if n != 1 {
			t.Fatalf("Bad read size: %v", err)
		}

		if Command(payload[0]) != e.cmd {
			t.Errorf("Bad content; expected %02x, got %02x", e.cmd, payload[0])
		}
	}
}

func TestMultiMsg(t *testing.T) {
	table := []struct {
		cmd Command
		out []byte
	}{
		{cmd: 0x50, out: []byte("\xaa\x55\x00\x00\x07\x00\x50\x57\x00")},
		{cmd: 0x52, out: []byte("\xaa\x55\x00\x00\x07\x00\x52\x59\x00")},
		{cmd: 0x56, out: []byte("\xaa\x55\x00\x00\x07\x00\x56\x5d\x00")},
		{cmd: 0xfe, out: []byte("\xaa\x55\x00\x00\x07\x00\xfe\x05\x01")},
	}

	// Append all messages into a single buffer
	all := make([][]byte, 0, len(table))
	for _, e := range table {
		all = append(all, e.out)
	}
	r := NewReader(bytes.NewReader(bytes.Join(all, []byte(""))))

	payload := make([]byte, 1)
	for _, e := range table {
		n, err := r.Read(payload)
		if err != nil {
			t.Fatal(err)
		}

		if n != 1 {
			t.Fatalf("Bad read size: %v", err)
		}

		if Command(payload[0]) != e.cmd {
			t.Errorf("Bad content; expected %02x, got %02x", e.cmd, payload[0])
		}
	}
}

func TestScan(t *testing.T) {
	table := []struct {
		cmd Command
		out []byte
	}{
		{cmd: 0x50, out: []byte("\xaa\x55\x00\x00\x07\x00\x50\x57\x00")},
		{cmd: 0x52, out: []byte("\xaa\x55\x00\x00\x07\x00\x52\x59\x00")},
		{cmd: 0x56, out: []byte("\xaa\x55\x00\x00\x07\x00\x56\x5d\x00")},
		{cmd: 0xfe, out: []byte("\xaa\x55\x00\x00\x07\x00\xfe\x05\x01")},
	}

	// Append all messages into a single buffer
	all := make([][]byte, 0, len(table))
	for _, e := range table {
		all = append(all, e.out)
	}
	scanner := NewScanner(bytes.NewReader(bytes.Join(all, []byte(""))))

	for _, e := range table {
		state := scanner.Scan()
		if !state {
			t.Fatal(scanner.Err())
		}

		payload := scanner.Payload()
		if len(payload) != 1 {
			t.Fatalf("Bad read size: %d", len(payload))
		}

		if Command(payload[0]) != e.cmd {
			t.Errorf("Bad content; expected %02x, got %02x", e.cmd, payload[0])
		}
	}
}

func TestReadErrors(t *testing.T) {
	table := []struct {
		pkt []byte
		err error
	}{
		{pkt: []byte("\xaa\x55\x00\x00\x07\x00\x56\x00\x00"),
			err: &InvalidChecksum{}},
	}

	payload := make([]byte, 1)
	for _, e := range table {
		r := NewReader(bytes.NewReader(e.pkt))
		_, err := r.Read(payload)
		if err == nil || err.Error() != e.err.Error() {
			t.Errorf("Error %T not detected (%v)", e.err, err)
		}
	}
}
