// Package ins supports the data messages from an Inertial Labs Inertial
// Navigation System (INS).
package ins

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"time"
)

// Integer scaling constants
const (
	GEOSCALE       float64 = 1e7
	GEOSCALE_HR    float64 = 1e9
	ANGLESCALE     float32 = 100
	ANGLESCALE_HR  float32 = 1000
	SPEEDSCALE     float32 = 100
	ALTSCALE       float32 = 100
	ALTSCALE_HR    float64 = 1000
	DISTSCALE      float32 = 100
	TEMPSCALE      float32 = 10
	PASCALE        float32 = 0.5
	MAGSCALE       float32 = 0.1
	GYROSCALE_HR   float64 = 1e5
	ACCELSCALE_HR  float64 = 1e6
	GYROBIASSCALE  float32 = 0.5e4
	ACCELBIASSCALE float32 = 0.5e5
	// For gyro range of 450 deg/s
	KGSCALE    float32 = 50
	KGSCALE_HR float64 = 1e4
	// For accelerometer range of +/- 8g
	KASCALE    float32 = 4000
	KASCALE_HR float64 = 1e4
)

const (
	CommandPacket uint8 = 0
	DataPacket    uint8 = 1
)

// INS commands
type Command uint8

const (
	GetDevInfo        Command = 0x12
	LoadParam         Command = 0x40
	ReadParam         Command = 0x41
	SensorsData       Command = 0x50
	OpvtData          Command = 0x52
	MinData           Command = 0x53
	NmeaData          Command = 0x54
	SensorsNmeaData   Command = 0x55
	QpvtData          Command = 0x56
	Opvt2aData        Command = 0x57
	Opvt2ahrData      Command = 0x58
	Opvt2awData       Command = 0x59
	OpvtadData        Command = 0x61
	AidingData        Command = 0x62
	RawImuData        Command = 0x66
	GnssExtData       Command = 0x67
	SpanRawImu        Command = 0x68
	UserDefData       Command = 0x95
	UserDefDataConfig Command = 0x96
	LoadParamRam      Command = 0xb0
	ReadParamRam      Command = 0xb1
	SaveParamFlash    Command = 0xb2
	SetOnReqMode      Command = 0xc1
	Stop              Command = 0xfe
	GetBit            Command = 0x1a
)

// Delay between sending a LoadParam command and sending the parameter
// data message.
const LoadParamDelay time.Duration = 5 * time.Millisecond

// Delay between configuring and starting the user-defined data
const UserDataDelay time.Duration = 1 * time.Second

// Device status word bits
type StatusWord uint16

const (
	BadAlignment StatusWord = (1 << iota)
	IncorrectIMU
	GyroFailure
	AccelFailure
	MagFailure
	ElecFailure
	GnssFailure
	OtfCalStatus
	LowVoltage
	HighVoltage
	XrateError
	YrateError
	ZrateError
	MagRangeExceeded
	TempRangeExceeded
	OtfCalOk
)

// Aiding data measurement codes
type AidingDataCode uint8

const (
	AdReserved AidingDataCode = iota
	AdOdometer
	AdAirSpeed
	AdWind
	AdExtPosition
	AdDoppler
	AdExtHeading
	AdDvl
)

// New_gps bit-field assignments
const (
	GnssPositionUpdate uint8 = (1 << iota)
	GnssVelUpdate
	GnssHeadingUpdate
	ValidPPS
	GnssBestXyzUpdate
	GnssPsrdopUpdate
	GnssLogUpdate
)

type opvtBase struct {
	Heading            uint16
	Pitch, Roll        int16
	Gyro               [3]int16
	Accel              [3]int16
	Mag                [3]int16
	Usw                StatusWord
	Vinp               uint16
	Temp               uint16
	Lat, Lon           int32
	Alt                int32
	E, N, U            int32
	Lat_gnss, Lon_gnss int32
	Alt_gnss           int32
	Hspeed             int32
	Track              uint16
	Vspeed             int32
	Ms_gps             int32
}

type opvtMessage struct {
	opvtBase
	Gnss_info   [2]uint8
	Svs         uint8
	Latency_pos uint8
	Latency_vel uint8
	Pbar        uint16
	Hbar        int32
	New_gps     uint8
}

type opvt2aMessage struct {
	opvtBase
	Gnss_info   [2]uint8
	Svs         uint8
	Vlatency    uint16
	Postype     uint8
	Hdg_gnss    uint16
	Latency_hdg int16
	Latency_pos int16
	Latency_vel int16
	Pbar        uint16
	Hbar        int32
	New_gps     uint8
}

// INS OPVT & Raw IMU Data format (see ICD manual section 6.2.9)
type opvtImuMessage struct {
	Ins_time, Imu_time     uint64
	Gyro                   [3]int32
	Accel                  [3]int32
	Usw                    StatusWord
	Heading                uint32
	Pitch, Roll            int32
	Lat, Lon               int64
	Alt                    int32
	E, N, U                int32
	Gnss_info1, Gnss_info2 uint8
	Svs                    uint8
	New_gps                uint8
}

// OpvtImu is the OPVT+IMU data converted to unscaled units
type OpvtImu struct {
	Tsec                   int64
	Tnsec                  int32
	ImuOffset              int64
	Gyro                   [3]float64
	Accel                  [3]float64
	Usw                    StatusWord
	Heading                float32
	Pitch, Roll            float32
	Lat, Lon               float64
	Alt                    float64
	E, N, U                float32
	Gnss_info1, Gnss_info2 uint8
	Svs                    uint8
	New_gps                uint8
}

func (o OpvtImu) Timestamp() time.Time {
	return time.Unix(o.Tsec, int64(o.Tnsec)).UTC()
}

const nsecPerSec int64 = int64(1e9)

func (o *OpvtImu) SetTimestamp(t time.Time) {
	ns := t.UnixNano()
	o.Tsec = ns / nsecPerSec
	o.Tnsec = int32(ns % nsecPerSec)
}

type OpvtBase struct {
	Timestamp          time.Time
	Heading            float32
	Pitch, Roll        float32
	Gyro               [3]float32
	Accel              [3]float32
	Mag                [3]float32
	Usw                StatusWord
	Vinp               float32
	Temp               float32
	Lat, Lon           float64
	Alt                float32
	E, N, U            float32
	Lat_gnss, Lon_gnss float64
	Alt_gnss           float32
	Hspeed             float32
	Track              float32
	Vspeed             float32
	Ms_gps             int32
}

// Opvt contains the OPVT data converted to unscaled units
type Opvt struct {
	OpvtBase
	Gnss_info   [2]uint8
	Svs         uint8
	Latency_pos uint8
	Latency_vel uint8
	Pbar        float32
	Hbar        float32
	New_gps     uint8
}

type Opvt2a struct {
	OpvtBase
	Gnss_info [2]uint8
	Svs       uint8
}

type csumMessage struct {
	CmdCsum uint16
}

type paramMessage struct {
	Rate                                   uint16
	Align_time                             uint16
	Mag_dec                                int32
	Lat, Lon                               int32
	Alt                                    int32
	Year, Month, Day                       uint8
	Align_a1, Align_a2, Align_a3           int16
	Mount_right, Mount_fwd, Mount_up       int16
	Antenna_right, Antenna_fwd, Antenna_up int16
	Alt2                                   uint8
	_                                      [8]uint8
	Name                                   [8]byte
	Baro_alt                               uint8
	_                                      uint8
}

func (pm paramMessage) size() int {
	return binary.Size(pm)
}

// Loc3d represents a 3-d location on the carrier vehicle
type Loc3d struct {
	Right   float32 `json:"right" toml:"right"`
	Forward float32 `json:"forward" toml:"forward"`
	Up      float32 `json:"up" toml:"up"`
}

// Parameters contains the parameters required for the INS initialization
type Parameters struct {
	// Output rate in Hz
	Rate uint `json:"rate" toml:"rate"`
	// Alignment time in seconds
	AlignTime uint `json:"talign" toml:"talign"`
	// Magnetic declination in degrees
	Declination float32 `json:"magdec" toml:"magdec"`
	// Declination reference date
	Date time.Time `json:"-" toml:"-"`
	// Initial location
	Lat float64 `json:"lat" toml:"lat"`
	Lon float64 `json:"lon" toml:"lon"`
	Alt float32 `json:"alt" toml:"alt"`
	// Alignment angles (see Appendix B of the ICD)
	Angles [3]float32 `json:"-" toml:"-"`
	// Mounting location
	Mount Loc3d `json:"mount" toml:"mount"`
	// Antenna location
	Antenna Loc3d `json:"antenna" toml:"antenna"`
	// Device name (read only)
	Name [8]byte `json:"-" toml:"-"`
	// Altimeter-altitude correction setting (see section 6.5 of the
	// INC ICD Manual).
	BaroAlt uint8 `json:"altcorr" toml:"altcorr"`
}

// ShortAlign is the short version of the initial alignment data sent by the INS
// after start-up.
type ShortAlign struct {
	// Gyro bias values in A/D counts
	GyroBias [3]float32 `json:"gyro_bias"`
	// Average acceleration in A/D counts
	AvgAccel [3]float32 `json:"avg_accel"`
	// Average magnetic field in A/D counts
	AvgMag [3]float32 `json:"avg_mag"`
	// Initial heading in degrees
	Heading float32 `json:"heading"`
	// Initial roll angle in degrees
	Roll float32 `json:"roll"`
	// Initial pitch angle in degrees
	Pitch float32 `json:"pitch"`
	// Unit status word
	Usw StatusWord `json:"usw"`
}

// Align contains the extended version of the initial alignment data.
type Align struct {
	ShortAlign
	// Pressure sensor temperature in A/D counts
	PrTemp int32 `json:"pr_temp"`
	// Pressure in A/D counts
	Pr int32 `json:"pr"`
	// Temperatures in the 3 gyros, 3 accelerometers, and 3 magnetometers
	// in A/D counts
	Temps [9]int16 `json:"temps"`
	// Latitude and Longitude in degrees
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
	// Altitude in m
	Alt float64 `json:"alt"`
	// East, north, and up velocities in m/s
	E float32 `json:"e"`
	N float32 `json:"n"`
	U float32 `json:"u"`
	// Gravity in m/s^2
	Gravity float64 `json:"gravity"`
	_       [2]float32
}

// Nul-padded ASCII strings
type ZString8 [8]byte
type ZString16 [16]byte
type ZString40 [40]byte

func (zs ZString8) MarshalJSON() ([]byte, error) {
	b := bytes.TrimRight(zs[:], "\x00")
	return json.Marshal(string(b))
}

func (zs ZString16) MarshalJSON() ([]byte, error) {
	b := bytes.TrimRight(zs[:], "\x00")
	return json.Marshal(string(b))
}

func (zs ZString40) MarshalJSON() ([]byte, error) {
	b := bytes.TrimRight(zs[:], "\x00")
	return json.Marshal(string(b))
}

type imuInfo struct {
	Type uint8     `json:"type"`
	Sn   ZString8  `json:"sn"`
	Fw   ZString40 `json:"fw"`
}

type gnssInfo struct {
	Model   ZString16 `json:"model"`
	Sn      ZString16 `json:"sn"`
	Hw      ZString16 `json:"hw"`
	Fw      ZString16 `json:"fw"`
	GpsWeek uint16    `json:"gps_week"`
	MaxRate uint8     `json:"max_rate"`
}

// DevInfo contains the response message from the GetDevInfo command.
type DevInfo struct {
	Sn       ZString8  `json:"sn"`
	Fw       ZString40 `json:"fw"`
	PrSensor uint8     `json:"prsensor"`
	Imu      imuInfo   `json:"imu"`
	Gnss     gnssInfo  `json:"gnss"`
	_        uint8
}

// Ship coordinate axes convention
const (
	StbdAxis int = 0
	FwdAxis      = 1
	MastAxis     = 2
)

// Units for the DVL latency measurement
const (
	dvlLatencyUnit time.Duration = time.Millisecond
	dvlMaxLatency                = 65536 * dvlLatencyUnit
	DVLSCALE       float32       = 1000
)

// DvlData contains Doppler Velocity Log aiding data
type DvlData struct {
	// Velocity data timestamp
	T time.Time
	// Velocity values in m/s
	V [3]float32
	// Velocity standard deviation in m/s
	Verr float32
	// Pressure at DVL in decibars
	Pr float32
}

// DvlAidingData contains the scaled values sent to the INS
type DvlAidingData struct {
	Vstbd, Vfwd, Vmast int32
	Estbd, Efwd, Emast uint16
	Lat                uint16
	Pr                 uint32
}

// Pack marshals the contents of the struct into the format expected
// by the INS. Tsample is the reference time to which the DVL sample
// latency is measured against, Tsample >= d.T
func (d DvlData) Pack(tsample time.Time) ([]byte, error) {
	offset := tsample.Sub(d.T)
	if offset < 0 || offset > dvlMaxLatency {
		return nil, fmt.Errorf("Invalid DVL latency: %v", offset)
	}

	rec := DvlAidingData{
		// Scaled velocity units are mm/s
		Vstbd: int32(d.V[StbdAxis] * DVLSCALE),
		Vfwd:  int32(d.V[FwdAxis] * DVLSCALE),
		Vmast: int32(d.V[MastAxis] * DVLSCALE),
		Estbd: uint16(d.Verr * DVLSCALE),
		Lat:   uint16(offset / dvlLatencyUnit),
		// Scaled pressure units are 10Pa
		Pr: uint32(d.Pr * 1000),
	}
	rec.Efwd = rec.Estbd
	rec.Emast = rec.Estbd

	var buf bytes.Buffer
	binary.Write(&buf, ByteOrder, rec)

	return buf.Bytes(), nil
}
