package ins

import "fmt"

// INS control parameter
type CtlParameter uint8

const (
	DataAverage         CtlParameter = 0x03
	AlignmentType                    = 0x04
	AllowAiding                      = 0x2c
	VelocityUseEnu                   = 0x34
	MaxINSTime                       = 0x57
	HeadingCorrection                = 0x58
	UseMagnetometers                 = 0x59
	AllowZupt                        = 0x5b
	VelocityConstraints              = 0x5c
	SensorOutput                     = 0x77
)

type LoadRamAck struct {
	ErrorCode uint8
	_         [2]uint8
}

type LoadRamError struct {
	code   CtlParameter
	reason uint8
}

func (e *LoadRamError) Error() string {
	return fmt.Sprintf("Parameter code %#x update failed: %#x",
		uint8(e.code), e.reason)
}

type HcType uint8

const (
	HcMagnetometers HcType = 0x0
	HcAhrs                 = 0x01
	HcGnssTrack            = 0x02
	HcDualGnss             = 0x03
	HcCombined             = 0x04
	HcInertial             = 0x05
)

type AlignmentSetting struct {
	T    uint16
	Type uint8
}

// Zero Velocity Update settings
type ZuptSetting struct {
	Enable bool
	// Minimum horizontal speed in m/s*100
	MinXySpeed uint8
	// Minimum vertical speed in ms*100
	MinZSpeed uint8
	// Low-pass filter time constant in s*100
	TConst uint8
}
