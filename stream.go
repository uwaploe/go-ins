package ins

import (
	"bytes"
	"context"
	"encoding/binary"
	"time"
)

// Take the device out of continuous mode.
func (d *Device) Stop() error {
	_, err := d.wtr.Write([]byte{uint8(Stop)})
	return err
}

// StartOpvtImu puts the device in continuous mode producing OPVT & Raw
// IMU data records. The return value is the initial alignment data which
// is measured before continuous mode starts. The initial alignment
// process takes up to 30 seconds so the timeout for device connection
// should be set accordingly.
func (d *Device) StartOpvtImu() (a Align, err error) {
	_, err = d.wtr.Write([]byte{uint8(RawImuData)})
	if err != nil {
		return a, err
	}

	err = d.getCommandAck()
	if err != nil {
		return a, err
	}

	// Wait for the initial alignment message.
	a, err = d.Alignment(false)
	return a, err
}

// StartUserData puts the device in continuous mode producing User
// Defined data records. The return value is the initial alignment data
// which is measured before continuous mode starts. The initial alignment
// process takes up to 30 seconds so the timeout for device connection
// should be set accordingly.
func (d *Device) StartUserData() (a Align, err error) {
	err = d.ConfigDataRecord()
	if err != nil {
		return a, err
	}
	time.Sleep(UserDataDelay)

	_, err = d.wtr.Write([]byte{uint8(UserDefData)})
	if err != nil {
		return a, err
	}

	err = d.getCommandAck()
	if err != nil {
		d.wtr.Write([]byte{uint8(UserDefData)})
		err = d.getCommandAck()
		return a, err
	}

	// Wait for the initial alignment message.
	a, err = d.Alignment(false)
	return a, err
}

// Return a channel which will produce the payload section of each message
// sent by the device. The device must be in continuous mode.
func (d *Device) StreamRecords(ctx context.Context) <-chan []byte {
	ch := make(chan []byte)
	go func() {
		defer close(ch)
		s := d.rdr.scanner
		for s.Scan() {
			select {
			case <-ctx.Done():
				return
			case ch <- s.Payload():
			default:
				// discard message
			}
		}
	}()

	return ch
}

// Return a channel which will produce the payload section of each message
// with an Id corresponding to id_match. The device must be in continuous mode.
func (d *Device) FilterRecords(ctx context.Context, id_match uint8) <-chan []byte {
	ch := make(chan []byte)
	go func() {
		defer close(ch)
		s := d.rdr.scanner
		for s.Scan() {
			select {
			case <-ctx.Done():
				return
			default:
			}

			if s.Id() == id_match {
				select {
				case ch <- s.Payload():
				default:
					// discard message
				}
			}
		}
	}()

	return ch
}

// Return a channel which will produce OpvtImu structs. The device must be
// in continuous mode.
func (d *Device) StreamOpvtImu(ctx context.Context) <-chan OpvtImu {
	ch := make(chan OpvtImu)
	go func() {
		defer close(ch)

		msg := opvtImuMessage{}
		for payload := range d.StreamRecords(ctx) {
			err := binary.Read(bytes.NewReader(payload), ByteOrder, &msg)
			if err != nil {
				continue
			}
			select {
			case ch <- UnpackOpvtImu(msg):
			default:
			}
		}
	}()

	return ch
}

// Return a channel which will produce DataRecord structs. The device must be
// in continuous mode.
func (d *Device) StreamDataRecord(ctx context.Context) <-chan DataRecord {
	ch := make(chan DataRecord)
	go func() {
		defer close(ch)

		for payload := range d.StreamRecords(ctx) {
			rec, err := UnpackUserData(payload)
			if err != nil {
				continue
			}
			select {
			case ch <- rec:
			default:
			}
		}
	}()

	return ch
}
