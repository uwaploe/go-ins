package ins

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"time"

	"github.com/pkg/errors"
)

type Device struct {
	rdr         *Reader
	wtr         *Writer
	paramUpdate bool
}

// NewDevice returns a new *Device instance
func NewDevice(rw io.ReadWriter) *Device {
	d := &Device{
		rdr: NewReader(rw),
		wtr: NewWriter(rw),
	}
	return d
}

// Parameters returns the current device parameters
func (d *Device) Parameters() (Parameters, error) {
	var p Parameters

	_, err := d.wtr.Write([]byte{uint8(ReadParam)})
	if err != nil {
		return p, err
	}

	msg := paramMessage{}

	// If this is the first read since the parameters were updated,
	// the device sends a checksum message. We need to read that and
	// resend the ReadParam command.
	if d.paramUpdate {
		d.paramUpdate = false
		cm := csumMessage{}
		binary.Read(d.rdr, ByteOrder, &cm)
		_, err = d.wtr.Write([]byte{uint8(ReadParam)})
	}

	err = binary.Read(d.rdr, ByteOrder, &msg)
	if err != nil {
		return p, err
	}
	p = UnpackParameters(msg)

	return p, err
}

// The device sends an acknowledgment message containing the
// calculated checksum of the command message.
func (d *Device) getCommandAck() error {
	cm := csumMessage{}
	err := binary.Read(d.rdr, ByteOrder, &cm)
	if err != nil {
		return errors.Wrap(err, "read ack")
	}

	if chk := d.wtr.GetChecksum(); chk != cm.CmdCsum {
		return &InvalidChecksum{}
	}

	return nil
}

// SetParameters loads new parameters into the device.
func (d *Device) SetParameters(p Parameters) error {
	pm := PackParameters(p)

	d.wtr.Write([]byte{uint8(LoadParam)})
	time.Sleep(LoadParamDelay)
	binary.Write(d.wtr, ByteOrder, pm)

	err := d.getCommandAck()
	if err != nil {
		return err
	}

	d.paramUpdate = true
	return nil
}

// Alignment returns the initial alignment data.
func (d *Device) Alignment(shortOnly bool) (ad Align, err error) {

	if shortOnly {
		err = binary.Read(d.rdr, ByteOrder, &ad.ShortAlign)
	} else {
		err = binary.Read(d.rdr, ByteOrder, &ad)
	}

	return
}

// DevInfo returns detailed device information
func (d *Device) DevInfo() (di DevInfo, err error) {
	_, err = d.wtr.Write([]byte{uint8(GetDevInfo)})
	if err != nil {
		return
	}

	err = binary.Read(d.rdr, ByteOrder, &di)
	return
}

// SendAirSpeed sends an air speed value in knots to the INS as aiding
// data. Before using this function, the COM1_Aiding_data parameter must
// be set to 1 using the INS Demo Program.
func (d *Device) SendAirSpeed(speed float64) error {
	payload := struct {
		Count  uint8
		Types  [1]uint8
		Values int16
	}{
		Count:  1,
		Types:  [1]uint8{uint8(AdAirSpeed)},
		Values: int16(speed * 100),
	}

	d.wtr.SetPacketId(uint8(AidingData))
	err := binary.Write(d.wtr, ByteOrder, payload)

	return err
}

// SendDvl sends Doppler Velocity log data to the INS. Before using this
// function, the COM1_Aiding_data parameter must be set to 1 using the INS
// Demo Program.
func (d *Device) SendDvl(rec DvlData) error {
	payload := struct {
		Count  uint8
		Types  [1]uint8
		Values []byte
	}{
		Count: 1,
		Types: [1]uint8{uint8(AdDvl)},
	}

	var err error
	payload.Values, err = rec.Pack(time.Now().UTC())
	if err != nil {
		return err
	}
	buf := make([]byte, len(payload.Values)+2)
	buf[0] = payload.Count
	buf[1] = payload.Types[0]
	copy(buf[2:], payload.Values)

	d.wtr.SetPacketId(uint8(AidingData))
	_, err = d.wtr.Write(buf)
	return err
}

// SetParameterRam sets a single INS control parameter
func (d *Device) SetParameterRam(code CtlParameter, val interface{}) error {
	var buf bytes.Buffer

	binary.Write(&buf, ByteOrder, uint8(LoadParamRam))
	binary.Write(&buf, ByteOrder, uint8(code))
	err := binary.Write(&buf, ByteOrder, val)
	if err != nil {
		return err
	}
	_, err = d.wtr.Write(buf.Bytes())
	if err != nil {
		return err
	}

	resp := LoadRamAck{}
	err = binary.Read(d.rdr, ByteOrder, &resp)
	if err != nil {
		return err
	}
	if resp.ErrorCode != 0 {
		return &LoadRamError{code: code, reason: resp.ErrorCode}
	}

	return nil
}

// ParameterRam reads the value of a single INS control parameter into data
func (d *Device) ParameterRam(code CtlParameter, data interface{}) error {
	_, err := d.wtr.Write([]byte{uint8(ReadParamRam), uint8(code)})
	if err != nil {
		return err
	}

	var rcode uint8
	err = binary.Read(d.rdr, ByteOrder, &rcode)
	if err != nil {
		return err
	}
	return binary.Read(d.rdr, ByteOrder, data)
}

// SaveFlash saves all parameter values to flash memory
func (d *Device) SaveFlash() error {
	_, err := d.wtr.Write([]byte{uint8(SaveParamFlash)})
	return err
}

// DataTypeError represents an error in the User Data definition
type DataTypeError struct {
	Code   uint8
	Bitmap uint16
}

func (e *DataTypeError) Error() string {
	return fmt.Sprintf("Data type error; code=%02x, bitmap=%016b",
		e.Code, e.Bitmap)
}

// ConfigDataRecord sets the format of the user-defined data record, see
// section 6.3.8 of the INS ICD.
func (d *Device) ConfigDataRecord() error {
	var buf bytes.Buffer
	rec := userDefMessage{}
	codes, size := TypeCodes(rec)
	if n := binary.Size(rec); n != size {
		return errors.Errorf("User data size mismatch; expected %d, got %d",
			n, size)
	}

	// Send command
	d.wtr.Write([]byte{uint8(UserDefDataConfig)})

	// Build configuration message contents
	buf.Write([]byte{uint8(len(codes))})
	buf.Write(codes)
	// Write it ...
	_, err := d.wtr.Write(buf.Bytes())
	if err != nil {
		return errors.Wrap(err, "write command")
	}

	// Configuration response
	resp := struct {
		Csum    uint16
		ListErr uint8
		TypeErr uint16
		MaxRate uint8
		_       [2]uint8
	}{}

	err = binary.Read(d.rdr, ByteOrder, &resp)
	if err != nil {
		return errors.Wrap(err, "read response")
	}

	if resp.Csum != d.wtr.GetChecksum() {
		return &InvalidChecksum{}
	}

	if (resp.ListErr & 0x07) != 0 {
		return &DataTypeError{Code: resp.ListErr, Bitmap: resp.TypeErr}
	}

	return nil
}
