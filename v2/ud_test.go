package ins

import (
	"encoding/binary"
	"testing"
)

func TestUserDataSize(t *testing.T) {
	rec := userDefMessage{}
	_, size := TypeCodes(rec)
	expected := binary.Size(rec)
	if size != expected {
		t.Errorf("Bad record size; expected %d, got %d", expected, size)
	}
}
