package ins

import (
	"time"
)

func PackParameters(p Parameters) paramMessage {
	pm := paramMessage{
		Rate:       uint16(p.Rate),
		Align_time: uint16(p.AlignTime),
		Mag_dec:    int32(p.Declination * ANGLESCALE),
		Lat:        int32(p.Lat * GEOSCALE),
		Lon:        int32(p.Lon * GEOSCALE),
		Alt:        int32(p.Alt * ALTSCALE),
		Year:       uint8(p.Date.Year() - 2000),
		Month:      uint8(p.Date.Month()),
		Day:        uint8(p.Date.Day()),
		Alt2:       1,
		Baro_alt:   p.BaroAlt,
	}

	copy(pm.Name[:], p.Name[:])
	pm.Align_a1 = int16(p.Angles[0] * ANGLESCALE)
	pm.Align_a2 = int16(p.Angles[1] * ANGLESCALE)
	pm.Align_a3 = int16(p.Angles[2] * ANGLESCALE)

	pm.Mount_right = int16(p.Mount.Right * DISTSCALE)
	pm.Mount_fwd = int16(p.Mount.Forward * DISTSCALE)
	pm.Mount_up = int16(p.Mount.Up * DISTSCALE)

	pm.Antenna_right = int16(p.Antenna.Right * DISTSCALE)
	pm.Antenna_fwd = int16(p.Antenna.Forward * DISTSCALE)
	pm.Antenna_up = int16(p.Antenna.Up * DISTSCALE)

	return pm
}

func UnpackParameters(pm paramMessage) Parameters {
	p := Parameters{
		Rate:        uint(pm.Rate),
		AlignTime:   uint(pm.Align_time),
		Declination: float32(pm.Mag_dec) / ANGLESCALE,
		Lat:         float64(pm.Lat) / GEOSCALE,
		Lon:         float64(pm.Lon) / GEOSCALE,
		Alt:         float32(pm.Alt) / ALTSCALE,
		BaroAlt:     pm.Baro_alt,
	}

	copy(p.Name[:], pm.Name[:])
	p.Date = time.Date(int(pm.Year)+2000, time.Month(pm.Month), int(pm.Day),
		0, 0, 0, 0, time.UTC)
	p.Angles[0] = float32(pm.Align_a1) / ANGLESCALE
	p.Angles[1] = float32(pm.Align_a2) / ANGLESCALE
	p.Angles[2] = float32(pm.Align_a3) / ANGLESCALE

	p.Mount.Right = float32(pm.Mount_right) / DISTSCALE
	p.Mount.Forward = float32(pm.Mount_fwd) / DISTSCALE
	p.Mount.Up = float32(pm.Mount_up) / DISTSCALE

	p.Antenna.Right = float32(pm.Antenna_right) / DISTSCALE
	p.Antenna.Forward = float32(pm.Antenna_fwd) / DISTSCALE
	p.Antenna.Up = float32(pm.Antenna_up) / DISTSCALE

	return p
}

func UnpackOpvtImu(msg opvtImuMessage) OpvtImu {
	// We need to determine the GPS week from the local clock since
	// the data message only contains the offset.
	week, _ := UtcToGps(time.Now())

	rec := OpvtImu{
		ImuOffset: int64(msg.Imu_time) - int64(msg.Ins_time),
		Usw:       msg.Usw,
		Heading:   float32(msg.Heading) / ANGLESCALE_HR,
		Pitch:     float32(msg.Pitch) / ANGLESCALE_HR,
		Roll:      float32(msg.Roll) / ANGLESCALE_HR,
		Lat:       float64(msg.Lat) / GEOSCALE_HR,
		Lon:       float64(msg.Lon) / GEOSCALE_HR,
		Alt:       float64(msg.Alt) / ALTSCALE_HR,
		E:         float32(msg.E) / SPEEDSCALE,
		N:         float32(msg.N) / SPEEDSCALE,
		U:         float32(msg.U) / SPEEDSCALE,
		Svs:       msg.Svs,
		New_gps:   msg.New_gps,
	}

	// Check for a GPS week boundary crossing
	// if time.Duration(msg.Ins_time) > offset {
	// 	week--
	// }

	(&rec).SetTimestamp(GpsToUtc(week, time.Duration(msg.Ins_time)))

	for i := 0; i < 3; i++ {
		rec.Gyro[i] = float64(msg.Gyro[i]) / KGSCALE_HR
		rec.Accel[i] = float64(msg.Accel[i]) / KASCALE_HR
	}

	rec.Gnss_info1 = msg.Gnss_info1
	rec.Gnss_info2 = msg.Gnss_info2

	return rec
}
