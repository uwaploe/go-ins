package ins

import (
	"testing"
	"time"
)

func TestTimeConv(t *testing.T) {
	ref := time.Now().UTC()
	week, offset := UtcToGps(ref)
	calc := GpsToUtc(week, offset)
	if !ref.Equal(calc) {
		t.Errorf("Time mismatch; expected %v, got %v", ref, calc)
	}
}

func TestDataRecordTime(t *testing.T) {
	rec := OpvtImu{}
	t0 := time.Date(2001, time.September, 9, 1, 46, 40, 42, time.UTC)
	(&rec).SetTimestamp(t0)
	t1 := rec.Timestamp()

	if !t1.Equal(t0) {
		t.Errorf("Time mismatch: %s != %s", t0, t1)
	}

	if rec.Tnsec != 42 {
		t.Errorf("Bad nanosecond value: %d", rec.Tnsec)
	}
}
