package ins

import (
	"bytes"
	"encoding/binary"
	"testing"
	"time"
)

func TestParamIo(t *testing.T) {
	var (
		b bytes.Buffer
		p Parameters
	)

	p.Rate = 20
	p.AlignTime = 30
	p.Mount.Forward = -1.5
	p.Date = time.Date(2001, time.January, 11, 0, 0, 0, 0, time.UTC)
	pm := PackParameters(p)
	w := NewWriter(&b)
	err := binary.Write(w, ByteOrder, pm)
	if err != nil {
		t.Fatal(err)
	}

	pm2 := paramMessage{}
	r := NewReader(&b)
	err = binary.Read(r, ByteOrder, &pm2)
	if err != nil {
		t.Fatal(err)
	}

	p2 := UnpackParameters(pm2)
	if p2 != p {
		t.Errorf("Data mismatch; expected %#v, got %#v", p, p2)
	}
}

func TestDvlPack(t *testing.T) {
	tsample := time.Date(2006, time.January, 2, 15, 4, 5, 0, time.UTC)
	table := []struct {
		rec    DvlData
		packed []byte
	}{
		{
			rec: DvlData{
				T:    tsample.Add(-time.Second),
				V:    [3]float32{1.024, 2.048, 3.072},
				Verr: 0.512,
				Pr:   32},
			packed: []byte{
				0x00, 0x04, 0x00, 0x00,
				0x00, 0x08, 0x00, 0x00,
				0x00, 0x0c, 0x00, 0x00,
				0x00, 0x02, 0x00, 0x02, 0x00, 0x02,
				0xe8, 0x03, 0x00, 0x7d, 0x00, 0x00,
			},
		},
	}

	for _, e := range table {
		b, err := e.rec.Pack(tsample)
		if err != nil {
			t.Fatal(err)
		}
		if string(b) != string(e.packed) {
			t.Errorf("Packing error; got %q, expected %q",
				string(b), string(e.packed))
		}
	}

	// Test DVL latency bounds
	table[0].rec.T = tsample.Add(time.Second)
	_, err := table[0].rec.Pack(tsample)
	if err == nil {
		t.Error("Bad DVL latency not caught")
	}

	table[0].rec.T = tsample.Add(-time.Hour)
	_, err = table[0].rec.Pack(tsample)
	if err == nil {
		t.Error("Bad DVL latency not caught")
	}
}
